$(function() {

// Load content
$('.content-link').click(function(e) {
	e.preventDefault();
	var url = $(this).prop('href');
	loadContent(url);
});
// End Load content

// Reset Layout
$('.my-name-label').click(function(e) {
	e.preventDefault();
	resetLayout();
});

/* Primary Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– */
	// Link Hover Event
	$('.content-link, .content-link-other').hover(function() {
		if (!$(this).hasClass('highlight'))
			$(this).toggleClass('hover');
		else
			$(this).removeClass('hover');
	});

	// Intelligent Highlighting
	$('#table-of-contents-bmi-1, #table-of-contents-bmi-2, #table-of-contents-craigslist').click(function(e) {
		e.preventDefault();
		$('.content-link').children('div').removeClass('highlight');
		$(this).addClass('highlight');
	});
	$('#table-of-contents-food-drink, #table-of-contents-grooveshark, #table-of-contents-vimeo').click(function(e) {
		e.preventDefault();
		$('.content-link').children('div').removeClass('highlight');
		$(this).addClass('highlight');
	});
	$('#table-of-contents-about-me-link, #table-of-contents-color-time, #table-of-contents-refindlist').click(function(e) {
		e.preventDefault();
		$('.content-link').children('div').removeClass('highlight');
		$(this).addClass('highlight');
	});
	
/* End Primary Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– */

/* Mobile Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– */
	// Toggle Menu
	$('#menu-toggle').click(function() {
		$('#actual-menu').toggleClass('display');
		if ($('#actual-menu').hasClass('display')) {
			$('#menu-icon').removeClass('fa-chevron-down')
			.addClass('fa-chevron-up');
		}
		else {
			$('#menu-icon').removeClass('fa-chevron-up')
			.addClass('fa-chevron-down');
		}
		$(this).toggleClass('highlight');
		$('#actual-menu').slideToggle(200);
	});
	// End Toggle Menu
		
	// Menu Icon Hover Event
	$('#menu-toggle').hover(
		function() { // mouse enter
			if ($('#actual-menu').hasClass('display')) {
				$('#menu-icon').removeClass('fa-chevron-down')
				.addClass('fa-chevron-up');
			}
		},
		function() { // mouse leave
			if ($('#actual-menu').hasClass('display')) {
				$('#menu-icon').removeClass('fa-chevron-up')
				.addClass('fa-chevron-down');
			}
		}
	);
	// End Menu Icon Hover Event

	// Menu Links Hover Event
	$('#menu-about-me-link').hover(function() {
		$('#mobile-file-icon').toggleClass('highlight');
	});
	$('#menu-email-link').hover(function() {
		$('#mobile-envelope-icon').toggleClass('highlight');
	});
	$('#menu-bitbucket-link').hover(function() {
		$('#mobile-bitbucket-icon').toggleClass('highlight');
	});
	// End Menu Links Hover Event

	// Icons Hover Event
	$('#mobile-file-icon').hover(function() {
		$('#menu-about-me-link').toggleClass('highlight');
	});
	$('#mobile-envelope-icon').hover(function() {
		$('#menu-email-link').toggleClass('highlight');
	});
	$('#mobile-bitbucket-icon').hover(function() {
		$('#menu-bitbucket-link').toggleClass('highlight');
	});
	// End Icons Hover Event

	// Menu Projects Hover Event
	$('#menu-bmi-1').hover(function() {
		$('#mobile-ss-cover-bmi-1').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-bmi-1').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-bmi-1').toggleClass('mobile-text-box-highlight');
	});
	$('#menu-bmi-2').hover(function() {
		$('#mobile-ss-cover-bmi-2').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-bmi-2').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-bmi-2').toggleClass('mobile-text-box-highlight');
	});
	$('#menu-color-time').hover(function() {
		$('#mobile-ss-cover-color-time').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-color-time').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-color-time').toggleClass('mobile-text-box-highlight');
	});
	$('#menu-craigslist').hover(function() {
		$('#mobile-ss-cover-craigslist').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-craigslist').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-craigslist').toggleClass('mobile-text-box-highlight');
	});
	$('#menu-food-drink').hover(function() {
		$('#mobile-ss-cover-food-drink').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-food-drink').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-food-drink').toggleClass('mobile-text-box-highlight');
	});
	$('#menu-grooveshark').hover(function() {
		$('#mobile-ss-cover-grooveshark').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-grooveshark').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-grooveshark').toggleClass('mobile-text-box-highlight');
	});
	$('#menu-refindlist').hover(function() {
		$('#mobile-ss-cover-refindlist').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-refindlist').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-refindlist').toggleClass('mobile-text-box-highlight');
	});
	$('#menu-vimeo').hover(function() {
		$('#mobile-ss-cover-vimeo').toggleClass('mobile-ss-cover-lowlight');
		$('#mobile-ss-cover-text-box-vimeo').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-vimeo').toggleClass('mobile-text-box-highlight');
	});
	// End Menu Projects Hover Event

	// Mobile Covers Hover Event
	$('#mobile-ss-cover-bmi-1').hover(function() {
		$('#menu-bmi-1').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-bmi-1').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-bmi-1').toggleClass('mobile-text-box-highlight');
	});
	$('#mobile-ss-cover-bmi-2').hover(function() {
		$('#menu-bmi-2').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-bmi-2').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-bmi-2').toggleClass('mobile-text-box-highlight');
	});
	$('#mobile-ss-cover-color-time').hover(function() {
		$('#menu-color-time').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-color-time').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-color-time').toggleClass('mobile-text-box-highlight');
	});
	$('#mobile-ss-cover-craigslist').hover(function() {
		$('#menu-craigslist').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-craigslist').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-craigslist').toggleClass('mobile-text-box-highlight');
	});
	$('#mobile-ss-cover-food-drink').hover(function() {
		$('#menu-food-drink').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-food-drink').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-food-drink').toggleClass('mobile-text-box-highlight');
	});
	$('#mobile-ss-cover-grooveshark').hover(function() {
		$('#menu-grooveshark').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-grooveshark').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-grooveshark').toggleClass('mobile-text-box-highlight');
	});
	$('#mobile-ss-cover-refindlist').hover(function() {
		$('#menu-refindlist').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-refindlist').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-refindlist').toggleClass('mobile-text-box-highlight');
	});
	$('#mobile-ss-cover-vimeo').hover(function() {
		$('#menu-vimeo').toggleClass('highlight');
		$('#mobile-ss-cover-text-box-vimeo').toggleClass('mobile-text-box-lowlight');
		$('#mobile-ss-cover-text-box-vimeo').toggleClass('mobile-text-box-highlight');
	});
	// End Mobile Covers Hover Event

	// Intelligent Highlighting
	$('#menu-bmi-1, #menu-bmi-2, #menu-color-time, #menu-craigslist, #menu-food-drink, \
		#menu-grooveshark, #menu-refindlist, #menu-vimeo, #menu-about-me-link, #mobile-file-icon').click(function(e) {
		e.preventDefault();
		var specialCase = false;
		if ($('#menu-about-me-link').hasClass('highlight'))
			specialCase = true;
		$('.content-link').children('div').removeClass('highlight');
		if (specialCase)
			$('#menu-about-me-link').addClass('highlight');
	});
	$('#menu-bmi-1, #mobile-ss-bmi-1').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-bmi-1').addClass('highlight');
	});
	$('#menu-bmi-2, #mobile-ss-bmi-2').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-bmi-2').addClass('highlight');
	});
	$('#menu-color-time, #mobile-ss-color-time').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-color-time').addClass('highlight');
	});
	$('#menu-craigslist, #mobile-ss-craigslist').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-craigslist').addClass('highlight');
	});
	$('#menu-food-drink, #mobile-ss-food-drink').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-food-drink').addClass('highlight');
	});
	$('#menu-grooveshark, #mobile-ss-grooveshark').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-grooveshark').addClass('highlight');
	});
	$('#menu-refindlist, #mobile-ss-refindlist').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-refindlist').addClass('highlight');
	});
	$('#menu-vimeo, #mobile-ss-vimeo').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-vimeo').addClass('highlight');
	});
	$('#menu-about-me-link, #mobile-file-icon').click(function(e) {
		e.preventDefault();
		$('#table-of-contents-about-me-link').addClass('highlight');
	});

/* End Mobile Layout
–––––––––––––––––––––––––––––––––––––––––––––––––– */
});
/* End Document Ready
–––––––––––––––––––––––––––––––––––––––––––––––––– */

/* Helper Functions
	–––––––––––––––––––––––––––––––––––––––––––––––––– */
function resetLayout() {
	$('.content-link').children('div').removeClass('highlight');
	$('.content').empty();
	$('.mobile-content').empty();
	$('#mobile-screenshot-container').fadeIn(400);
}

function loadContent(url) {
	$('#mobile-screenshot-container').fadeOut(400);
	$('.content').load(url);
	$('.mobile-content').load(url);
}
/* End Helper Functions
	–––––––––––––––––––––––––––––––––––––––––––––––––– */