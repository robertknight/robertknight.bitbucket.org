$(function() {
	var english_button = $('#english_button');
	var metric_button = $('#metric_button');
	var english_units = $('#english_units');
	var metric_units = $('#metric_units');
	
	var input_ft = $('#ft');
	var input_in = $('#in');
	var input_lb = $('#lb');
	var input_cm = $('#cm');
	var input_kg = $('#kg');
	var submit_button = $('#submit_button');

	english_button.click(function() {
		$(this).addClass('active btn-primary');

		metric_button.removeClass('active btn-primary');
		metric_button.addClass('btn-default');

		clear();
		
		metric_units.hide();
		english_units.show();
	});
	
	metric_button.click(function() {
		$(this).addClass('active btn-primary');

		english_button.removeClass('active btn-primary');
		english_button.addClass('btn-default');

		clear();
		
		english_units.hide();
		metric_units.show();
	});

	submit_button.click(function(e) {
		e.preventDefault();

		if (english_button.hasClass('active') && isValidEnglishInput())
		{
			var weight = parseInt(input_lb.val());
			var height = parseInt(12 * input_ft.val()) + parseInt(input_in.val());
			
			var BMI = weight / Math.pow(height, 2) * 703.06957964;
			
			printBMI(BMI);
		}
		else if (metric_button.hasClass('active') && isValidMetricInput())
		{
			var mass = parseInt(input_kg.val());
			var height = parseFloat(input_cm.val() / 100);

			var BMI = mass / Math.pow(height, 2);

			printBMI(BMI);
		}
	});

	function clear() {
		$('input').each(function() {
			$(this).val('');
		});
		$('#result, #status').text('');
	}

	function isValidEnglishInput() {
		if (input_ft.length > 0 && input_in.length > 0 && input_lb.length > 0 && 
			$.isNumeric(input_ft.val()) && $.isNumeric(input_in.val()) && $.isNumeric(input_lb.val()))
				return true;
		else
		{
			$('#result, #status').text('');
			return false;
		}
	}

	function isValidMetricInput() {
		if (input_cm.length > 0 && input_kg.length > 0 && 
			$.isNumeric(input_cm.val()) && $.isNumeric(input_kg.val()))
				return true;
		else
		{
			$('#result, #status').text('');
			return false;
		}
	}

	function printBMI(n) {
		var BMI = (Math.round(n * 100) / 100).toFixed(1);

		$('#result').text(BMI);
		
		if (BMI < 18.5)
			$('#status').text('Underweight');
		else if (BMI >= 18.5 && BMI < 25)
			$('#status').text('Normal');
		else if (BMI >= 25 && BMI < 30)
			$('#status').text('Overweight');
		else
			$('#status').text('Obese');
	}
});