$(function() {
	$('#search_button').click(function(event) {
		event.preventDefault();

		var input = $('#vimeo_input').val();
		
		if (input.length > 0)
		{
			input = encodeURIComponent(input);

			$.get(
				'my.php',
				{	query: input },
				function(resp) {
					console.log(resp);
					resp.body.data.forEach(function(obj) {
						var video_id = obj.uri.substring(8, obj.uri.length);
						$('#video_box').append(
							'<span class="vimeo_video"> \
								<iframe src="//player.vimeo.com/video/' + video_id + '" width="250" height="140" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe> \
							</span>'
						);
					});
				},
				'json'
			);
		}
	});

	$('#remove_button').click(function(event) {
		event.preventDefault();

		$('#vimeo_input').val('');
		$('.vimeo_video').remove();
	});
});