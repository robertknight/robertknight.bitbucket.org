<?php
	require_once('vimeo.php');

	$config = json_decode(file_get_contents('vimeo_config.json'), true);

	const ITEM_LIMIT = 8;

	$query = isset($_GET['query']) ? $_GET['query'] : null;

	if ($query)
		$query = utf8_encode($query);

	$vimeo = new Vimeo($config['APP_ID'], $config['APP_SECRET']);
	$vimeo->setToken($config['ACCESS_TOKEN']);

	$videos = $vimeo->request('/videos', ['per_page' => ITEM_LIMIT, 'query' => $query]);

	echo json_encode($videos);