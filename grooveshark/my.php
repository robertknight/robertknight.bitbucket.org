<?php

	$config = json_decode(file_get_contents('grooveshark_config.json'), true);

	const LIMIT = 10;

	$query = isset($_GET['query']) ? $_GET['query'] : null;

	$url = utf8_encode('http://tinysong.com/s/'.$query.'?format=json&limit='.LIMIT.'&key='.$config['APP_KEY']);

	$file = file_get_contents($url);

	echo $file;