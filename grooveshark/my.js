$(function() {
	$('#search_button').click(function() {
		var input = $('#gs_input').val();
		
		if (input.length > 0)
		{
			input = encodeURIComponent(input);

			$.get(
				'my.php',
				{	query: input },
				function(resp) {
					resp.forEach(function(obj) {
						// Url: obj.Url
						// SongID: obj.SongID 
						// SongName: obj.SongName 
						// ArtistID: obj.ArtistID 
						// ArtistName: obj.ArtistName 
						// AlbumID: obj.AlbumID 
						// AlbumName: obj.AlbumName
						$('#song_box').append(
							'<div class="gs_player_element"> \
								<object width="250" height="40" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" id="gsSong' + obj.SongID + '"name="gsSong' + obj.SongID + '"><param name="movie" value="//grooveshark.com/songWidget.swf" /><param name="wmode" value="window" /><param name="allowScriptAccess" value="always" /><param name="flashvars" value="hostname=grooveshark.com&songID=' + obj.SongID + '&style=water&p=0"></object> \
								<object class="gs_player" type="application/x-shockwave-flash" data="//grooveshark.com/songWidget.swf" height="40"><param name="wmode" value="window" /><param name="allowScriptAccess" value="always" /><param name="flashvars" value="hostname=grooveshark.com&songID='+ obj.SongID +'&style=water&p=0" /><span><a href="' + obj.Url + '" title="' + obj.SongName + ' by ' + obj.ArtistName + ' on Grooveshark">' + obj.SongName + ' by ' + obj.ArtistName + ' on Grooveshark</a></span></object> \
							</div>'
						);
					});
				},
				'json'
			);
		}
		return false;
	});

	$('#remove_button').click(function() {
		$('#gs_input').val('');
		$('.gs_player_element').remove();
		return false;
	});
});