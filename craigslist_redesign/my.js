$(function() {
	var options = {
		community: 'ccc',
		events: 'eee',
		gigs: 'ggg',
		housing: 'hhh',
		jobs: 'jjj',
		personals: 'ppp',
		resumes: 'rrr',
		'for sale': 'sss',
		services: 'bbb'
	};

	$('#search_button').click(function(event) {	
		event.preventDefault();
		var input = $('#cl_input').val();
		if (input.length > 0)
		{
			category = options[$('select').val()];
			window.location.href = encodeURI('//sacramento.craigslist.org/search/'+category+'?query='+input+'&sort=rel');
		}
	});
});